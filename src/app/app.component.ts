import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  tree_first: any = {
    value: 99,
    right: [
      {
        value: 45,
        left: [
          {
            value: 32,
            left: [
              {
                value: 90,
                left: [
                  {
                    value: 23,
                    left: [],
                    right: []
                  }
                ],
                right: [
                  {
                    value: 12,
                    left: [],
                    right: []
                  }
                ]
              }
            ],
            right: []
          }
        ],
        right: [
          {
            value: 34,
            left: [],
            right: [
              {
                value: 10,
                left: [
                  {
                    value: 34,
                    left: [],
                    right: []
                  }
                ],
                right: [
                  {
                    value: 78,
                    left: [],
                    right: []
                  }
                ]
              }
            ]
          }
        ]
      }
    ],
    left: [
      {
        value: 56,
        left: [
          {
            value: 5,
            left: [
              {
                value: 67,
                left: [
                  {
                    value: 89,
                    left: [],
                    right: []
                  }
                ],
                right: [
                  {
                    value: 59,
                    left: [],
                    right: []
                  }
                ]
              }
            ],
            right: [
              {
                value: 56,
                left: [{
                  value: 98,
                  left: [],
                  right: []
                }],
                right: [
                  {
                    value: 23,
                    left: [],
                    right: []
                  }
                ]
              }
            ]
          }
        ],
        right: []
      }
    ]
  }

  travel: any = [];
  travelFull: any = [];
  pilaLifo: any = [];

  searchForm: FormGroup;
  numberToSearch: any = '';
  showMessageInfo: any = false;
  showMessageDanger: any = false;
  finishTravel: any = false;

  constructor(private formBuilder: FormBuilder) {
    this.searchForm = this.formBuilder.group({
      numberSearch: ['', [Validators.required]]
    });
  }

  ngOnInit() { }

  searchNumberInTree() {
    console.log("arbol", this.tree_first)
    this.travel = [];
    this.pilaLifo = [];
    this.travelFull = [];
    this.showMessageInfo = false;
    this.showMessageDanger = false;
    this.finishTravel = false;

    this.numberToSearch = this.searchForm.value.numberSearch;
    this.pushPila(this.tree_first);
    this.popPila();
  }


  pushPila(element: any) {
    this.travelFull.push(element.value);
    this.pilaLifo.push(element);
  }

  popPila() {
    if (this.pilaLifo.length > 0) {
      let element = JSON.parse(JSON.stringify(this.pilaLifo[this.pilaLifo.length - 1]));
      this.pilaLifo.splice(-1, 1);
      this.travelFull.push(element.value);

      // let childrenRight = element.right.length;
      // let childrenLeft = element.left.length;

      // if (childrenRight > 0) {
      //   this.pushPila(element.right[0]);
      // }

      // if (childrenLeft > 0) {
      //   this.pushPila(element.left[0]);
      // }

      this.evaluateNodo(element);
    } else {
      this.showMessageInfo = false;
      this.showMessageDanger = true;
      this.finishTravel = true;
    }
  }

  compareNumber(number) {
    let value = this.searchForm.value.numberSearch;
    return number == value ? true : false;
  }

  evaluateNodo(nodo: any) {
    this.travel.push(nodo.value);
    let searchNumber = this.compareNumber(nodo.value);
    if (!searchNumber) {

      let childrenRight = nodo.right.length;
      let childrenLeft = nodo.left.length;

      if (childrenRight > 0) {
        this.pushPila(nodo.right[0]);
      }

      if (childrenLeft > 0) {
        this.pushPila(nodo.left[0]);
      }

      this.popPila();
    } else {
      this.showMessageInfo = true;
      this.showMessageDanger = false;
      this.finishTravel = true;
    }
  }

  formControls(formControlName: string) {
    return this.searchForm.controls[formControlName];
  }


  validateControl(control) {
    const formControl = this.searchForm.controls[control];
    return (formControl.invalid && (formControl.dirty || formControl.touched));
  }


}
